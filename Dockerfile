# Spring boot docker file

FROM gradle:6.5.1-jdk11 AS build
COPY --chown=gradle:gradle . /home/gradle/src
WORKDIR /home/gradle/src

# will run build witout test stage - should be improved for real project.
RUN gradle build --no-daemon -x test

FROM openjdk:11

EXPOSE 8080

RUN mkdir /app

COPY --from=build /home/gradle/src/build/libs/*.jar /app/spring-boot-application.jar

ENTRYPOINT ["java", "-jar","/app/spring-boot-application.jar", \
            "--spring.config.location=classpath:application.properties,\
                                      classpath:properties/whishlist.properties"]