<?php

namespace App\Commands;

use App\Exceptions\ArgumentNotFoundException;
use Illuminate\Console\Scheduling\Schedule;
use Illuminate\Support\Facades\Storage;
use LaravelZero\Framework\Commands\Command;
use Illuminate\Support\Facades\DB;

class ExportWhishlist extends Command
{
    /**
     * The signature of the command.
     *
     * @var string
     */
    protected $signature = 'export {out=whishlists.csv} {--pageSize=100}';

    /**
     * The description of the command.
     *
     * @var string
     */
    protected $description = 'Exports data from whishlists db into csv files.
                Data fetch is performed by chunks so you can specify pageSize option
                to handle db performance (default 100 rows)';

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {

        $outputFile = $this->argument("out");
        $pageSize = $this->option("pageSize");

        $whishlistsCount = DB::table("whishlists")->count("id");

        $chunksCount = ceil($whishlistsCount / $pageSize);

        Storage::delete($outputFile);
        $outputFileHandler = fopen('./' . Storage::url($outputFile), 'a+');

        $columns  = ['user', 'title whishlist', 'number of items'];

        fputcsv($outputFileHandler, $columns);

        for($i = 0; $i < $chunksCount; $i++) {
            $result = $this->fetchwhishlists($i, $pageSize);

            $this->info("progress: " . 100 * $i / $chunksCount . "%");

            foreach ($result as $row) {
                fputcsv($outputFileHandler, [$row->username, $row->title, $row->products_count]);
            }
        }

        fclose($outputFileHandler);

        $this->info("Successfully exported data into file: " . Storage::url($outputFile));

        return;
    }

    /**
     * Fetches whishlists from db in the following format:
     * {"username":"test","title":"something new","products_count":4}
     * potentially instead of username could be user id.
     *
     * @param $page
     * @param $pagSize
     * @return \Illuminate\Support\Collection
     */
    private function fetchwhishlists($page, $pagSize) {
        return DB::table("whishlists")
            ->join("users", "users.id", '=', "whishlists.user_id")
            ->join("whishlists_product_list", "whishlists.id", "=", "whishlists_product_list.whishlist_id")
            ->select("users.username", "whishlists.title",
                DB::raw("count(whishlists_product_list.product_list_id) as products_count"))
            ->groupBy("users.username", "whishlists.title", "whishlists.id")
            ->orderBy("whishlists.id", "DESC")
            ->skip($page * $pagSize)->take($pagSize)
            ->get();
    }

    /**
     * Define the command's schedule.
     *
     * @param  \Illuminate\Console\Scheduling\Schedule $schedule
     * @return void
     */
    public function schedule(Schedule $schedule): void
    {
        // $schedule->command(static::class)->everyMinute();
    }
}
