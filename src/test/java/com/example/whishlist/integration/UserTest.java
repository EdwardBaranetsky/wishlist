package com.example.whishlist.integration;

import com.example.whishlist.entity.User;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.boot.web.server.LocalServerPort;
import org.springframework.http.*;
import org.springframework.test.context.TestPropertySource;

import java.util.Objects;

import static org.junit.jupiter.api.Assertions.assertTrue;

@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
@TestPropertySource("classpath:whishlist_test.properties")
public class UserTest {

    private static TestRestTemplate restTemplate = new TestRestTemplate();

    @LocalServerPort
    private int port;

    private HttpHeaders headers;
    private User defaultUser;

    @Value("${HOME_PAGE_GREETING}")
    private String homePageGreeting;

    @BeforeEach
    public void before() {
        headers = TestUtil.createJsonHeaders();
        defaultUser = TestUtil.createDefaultUser();
    }

    @AfterEach
    public void after() {
        TestUtil.removeUser(defaultUser, headers, port);
    }

    @Test
    public void registerUserTest() {
        ResponseEntity<String> response = TestUtil.registerUser(defaultUser, headers, port);

        long userId = Long.parseLong(Objects.requireNonNull(response.getBody()));

        assertTrue(userId > 0);
    }

    @Test
    public void loginUserTest() {
        TestUtil.registerUser(defaultUser, headers, port);

        HttpEntity<User> entity = new HttpEntity<>(defaultUser, headers);

        ResponseEntity<String> response =
                restTemplate.withBasicAuth(defaultUser.getUsername(), defaultUser.getPassword())
                        .exchange(TestUtil.createURLWithPort(port, "/"), HttpMethod.GET, entity, String.class);

        assertTrue(Objects.requireNonNull(response.getBody()).contains(homePageGreeting));
    }
}
