package com.example.whishlist.integration;

import com.example.whishlist.entity.Product;
import com.example.whishlist.entity.User;
import com.example.whishlist.entity.Whishlist;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.boot.web.server.LocalServerPort;
import org.springframework.http.*;
import org.springframework.test.context.TestPropertySource;
import org.springframework.util.LinkedMultiValueMap;
import org.springframework.util.MultiValueMap;
import org.springframework.web.util.UriComponentsBuilder;

import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

import static org.junit.jupiter.api.Assertions.*;

@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
@TestPropertySource("classpath:whishlist_test.properties")
public class WhishlistTest {

    private static TestRestTemplate restTemplate = new TestRestTemplate();
    private static Gson gson = new Gson();

    @LocalServerPort
    private int port;

    private HttpHeaders headers;
    private User defaultUser;
    private ProductRestManager productRestManager;

    @BeforeEach
    public void before() {
        headers = TestUtil.createJsonHeaders();
        defaultUser = TestUtil.createDefaultUser();
        TestUtil.registerUser(defaultUser, headers, port);
        productRestManager = new ProductRestManager(defaultUser, port);
    }

    @AfterEach
    public void after() {
        TestUtil.removeUser(defaultUser, headers, port);
    }

    @Test
    public void createWhishlist() {
        productRestManager.createProducts(headers,
                Product.builder().name("milk").build(),
                Product.builder().name("sony playstation 4").build());

        List<Product> productList = productRestManager.selectLatestProducts(headers, 2);

        Whishlist whishlist = createWhishlist("whishlist for test user", productList);

        assertTrue(whishlist.getId() > 0);

        removeWhishlistById(whishlist.getId());
    }

    @Test
    public void showWhishlistTest() {
        productRestManager.createProducts(headers,
                Product.builder().name("bred").build(),
                Product.builder().name("water").build(),
                Product.builder().name("eggs").build());

        List<Product> productList = productRestManager.selectLatestProducts(headers, 3);
        List<Whishlist> whishlists = new ArrayList<>();

        Whishlist whishlist = createWhishlist("whishlist for breakfast", productList);
        whishlists.add(whishlist);

        productRestManager.createProducts(headers,
                Product.builder().name("soup").build(),
                Product.builder().name("ice-cream").build());

        productList = productRestManager.selectLatestProducts(headers, 2);

        whishlist = createWhishlist("whishlist for dinner", productList);
        whishlists.add(whishlist);


        UriComponentsBuilder uriBuilder = UriComponentsBuilder
                .fromUriString(TestUtil.createURLWithPort(port, "/whishlist/"))
                .queryParam("page", 0)
                .queryParam("pageSize", 10);

        HttpEntity entity = new HttpEntity<>(headers);

        ResponseEntity<String> response = restTemplate.withBasicAuth(defaultUser.getUsername(), defaultUser.getPassword())
                .exchange(uriBuilder.toUriString(), HttpMethod.GET, entity, String.class);

        Type listType = new TypeToken<List<Whishlist>>(){}.getType();
        List<Whishlist> fetchedWhishlist = gson.fromJson(response.getBody(), listType);

        assertNotNull(fetchedWhishlist);
        assertTrue(getWhishlistsTitles(whishlists)
                        .containsAll(getWhishlistsTitles(fetchedWhishlist)));

        fetchedWhishlist.forEach(whish -> removeWhishlistById(whish.getId()));
    }

    @Test
    public void addProductAndRemoveProductTest() {
        productRestManager.createProducts(headers, Product.builder().name("nuts").build());

        List<Product> productList = productRestManager.selectLatestProducts(headers, 1);
        Whishlist whishlist = createWhishlist("whishlist for breakfast", productList);

        ResponseEntity<String> response = productRestManager.createProduct(headers,
                Product.builder().name("meat").build());

        Product product = gson.fromJson(response.getBody(), Product.class);

        MultiValueMap<String, String> map= new LinkedMultiValueMap<>();
        map.add("whishlistId", whishlist.getId().toString());
        map.add("productId", product.getId().toString());

        HttpHeaders headers = new HttpHeaders();
        headers.setContentType(MediaType.APPLICATION_FORM_URLENCODED);
        HttpEntity<MultiValueMap<String, String>> request = new HttpEntity<>(map, headers);

        response = restTemplate.withBasicAuth(defaultUser.getUsername(), defaultUser.getPassword())
                .postForEntity(TestUtil.createURLWithPort(port, "/whishlist/addProduct"), request, String.class);

        whishlist = gson.fromJson(response.getBody(), Whishlist.class);

        assertTrue(whishlist.getProductList()
                .stream().map(Product::getName)
                .collect(Collectors.toList())
                .contains(product.getName()));

        response = restTemplate.withBasicAuth(defaultUser.getUsername(), defaultUser.getPassword())
                .postForEntity(TestUtil.createURLWithPort(port, "/whishlist/removeProduct"), request, String.class);

        whishlist = gson.fromJson(response.getBody(), Whishlist.class);

        assertEquals(1, whishlist.getProductList().size());

        removeWhishlistById(whishlist.getId());
    }

    private Whishlist createWhishlist(String title, List<Product> productList) {
        Whishlist whishlist = Whishlist.builder()
                .title(title)
                .productList(productList)
                .user(defaultUser)
                .build();

        HttpEntity<Whishlist> entity = new HttpEntity<>(whishlist, headers);

        ResponseEntity<String> response = restTemplate.withBasicAuth(defaultUser.getUsername(), defaultUser.getPassword())
                .exchange(TestUtil.createURLWithPort(port, "/whishlist/create"), HttpMethod.POST, entity, String.class);

        assertNotNull(response.getBody());

        return gson.fromJson(response.getBody(), Whishlist.class);
    }

    private List<String> getWhishlistsTitles(List<Whishlist> whishlists) {
        return whishlists.stream().map(Whishlist::getTitle).collect(Collectors.toList());
    }


    private void removeWhishlistById(Long whishlistId) {
        MultiValueMap<String, String> map= new LinkedMultiValueMap<>();
        map.add("whishlistId", whishlistId.toString());

        HttpHeaders headers = new HttpHeaders();
        headers.setContentType(MediaType.APPLICATION_FORM_URLENCODED);
        HttpEntity<MultiValueMap<String, String>> request = new HttpEntity<>(map, headers);

        ResponseEntity<String> response = restTemplate.withBasicAuth(defaultUser.getUsername(), defaultUser.getPassword())
                .postForEntity(TestUtil.createURLWithPort(port, "/whishlist/remove"), request, String.class);

        assertEquals(HttpStatus.OK, response.getStatusCode());
    }
}
