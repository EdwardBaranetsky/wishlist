package com.example.whishlist.integration;

import com.example.whishlist.entity.Product;
import com.example.whishlist.entity.User;

import com.google.gson.Gson;

import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.boot.web.server.LocalServerPort;
import org.springframework.http.*;
import org.springframework.test.context.TestPropertySource;

import java.util.List;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;

@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
@TestPropertySource("classpath:whishlist_test.properties")
public class ProductTest {

    private static TestRestTemplate restTemplate = new TestRestTemplate();

    @LocalServerPort
    private int port;

    private HttpHeaders headers;
    private User defaultUser;
    private Gson gson;
    private ProductRestManager restManager;

    @BeforeEach
    public void before() {
        headers = TestUtil.createJsonHeaders();
        defaultUser = TestUtil.createDefaultUser();
        gson = new Gson();
        TestUtil.registerUser(defaultUser, headers, port);
        restManager = new ProductRestManager(defaultUser, port);
    }

    @AfterEach
    public void after() {
        TestUtil.removeUser(defaultUser, headers, port);
    }

    @Test
    public void createProductTest() {
        Product product = Product.builder().name("chocolate").price(100).build();

        ResponseEntity<String> response = restManager.createProduct(headers, product);

        product = gson.fromJson(response.getBody(), Product.class);

        assertNotNull(product);
    }

    @Test
    public void showProductListTest() {
        int pageSize = 10;
        for (int i=0; i < pageSize; i++) {
            Product product = Product.builder().name("chocolate " + i).price(100 + i).build();
            restManager.createProduct(headers, product);
        }

        List<Product> products = restManager.selectProducts(headers, 0, pageSize);

        assertNotNull(products);
        assertEquals(pageSize, products.size());
    }
}
