package com.example.whishlist.integration;

import com.example.whishlist.entity.User;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.http.*;

import java.util.Objects;

import static org.junit.jupiter.api.Assertions.assertEquals;

/**
 * Helper class for user creation and local port resolution.
 */
public class TestUtil {

    private static TestRestTemplate restTemplate = new TestRestTemplate();


    public static void removeUser(User user, HttpHeaders headers, int port) {
        HttpEntity<User> entity = new HttpEntity<>(user, headers);

        ResponseEntity<String> response = restTemplate.exchange(
                createURLWithPort(port,"/user/remove"), HttpMethod.POST, entity, String.class);

        assertEquals(response.getStatusCode(), HttpStatus.OK);
    }

    public static ResponseEntity<String> registerUser(User user, HttpHeaders headers, int port) {
        HttpEntity<User> entity = new HttpEntity<>(user, headers);

        ResponseEntity<String> response = restTemplate.exchange(
                createURLWithPort(port, "/register"), HttpMethod.POST, entity, String.class);

        assertEquals(response.getStatusCode(), HttpStatus.OK);

        user.setId(Long.parseLong(Objects.requireNonNull(response.getBody())));

        return response;
    }

    public static String createURLWithPort(int port, String uri) {
        return "http://127.0.0.1:" + port + uri;
    }

    public static User createDefaultUser() {
        return User.builder().username("test").password("123").confirmPassword("123").build();
    }

    public static HttpHeaders createJsonHeaders() {
        HttpHeaders headers = new HttpHeaders();
        headers.setContentType(MediaType.APPLICATION_JSON);
        return headers;
    }
}
