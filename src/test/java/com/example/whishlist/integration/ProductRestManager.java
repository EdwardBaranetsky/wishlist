package com.example.whishlist.integration;

import com.example.whishlist.entity.Product;
import com.example.whishlist.entity.User;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.http.*;
import org.springframework.web.util.UriComponentsBuilder;

import java.lang.reflect.Type;
import java.util.List;

/**
 * It is helper class for working with products over rest api.
 * todo: add products removal
 */
public class ProductRestManager {

    private static TestRestTemplate restTemplate = new TestRestTemplate();

    private User user;
    private int port;
    private Gson gson;

    public ProductRestManager(User user, int port) {
        this.user = user;
        this.port = port;
        this.gson = new Gson();
    }

    public ResponseEntity<String> createProduct(HttpHeaders headers, Product product) {
        HttpEntity<Product> entity = new HttpEntity<>(product, headers);

        return restTemplate.withBasicAuth(user.getUsername(), user.getPassword())
                .exchange(TestUtil.createURLWithPort(port, "/product/create"), HttpMethod.POST, entity, String.class);
    }

    public void createProducts(HttpHeaders headers, Product... products) {
        for (Product product : products) {
            ResponseEntity<String> response = createProduct(headers, product);
            if (!response.getStatusCode().equals(HttpStatus.OK)) {
                throw new IllegalStateException(
                        String.format("Product %s was not created cause: %s", product, response.getBody()));
            }
        }
    }

    public List<Product> selectProducts(HttpHeaders headers, int page, int pageSize) {

        UriComponentsBuilder uriBuilder = UriComponentsBuilder
                .fromUriString(TestUtil.createURLWithPort(port, "/product/list"))
                .queryParam("page", page)
                .queryParam("pageSize", pageSize);

        HttpEntity entity = new HttpEntity<>(headers);

        ResponseEntity<String> response = restTemplate.withBasicAuth(user.getUsername(), user.getPassword())
                .exchange(uriBuilder.toUriString(), HttpMethod.GET, entity, String.class);

        Type listType = new TypeToken<List<Product>>(){}.getType();
        return gson.fromJson(response.getBody(), listType);
    }

    public List<Product> selectLatestProducts(HttpHeaders headers, int pageSize) {
        return selectProducts(headers, 0, pageSize);
    }
}
