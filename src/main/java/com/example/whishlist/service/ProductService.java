package com.example.whishlist.service;

import com.example.whishlist.entity.Product;
import com.example.whishlist.repository.ProductRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class ProductService {

    @Autowired
    private ProductRepository repository;

    public Product createProduct(Product product) {
        if (repository.existsProductByName(product.getName())) {
            throw new IllegalArgumentException("Product with specified name already exists");
        }

        return repository.save(product);
    }

    public Product getProductById(long id) {
        return repository.getProductById(id);
    }

    public List<Product> selectProductList(int page, int pageSize) {
        return repository.findAllBy(PageRequest.of(page, pageSize, Sort.Direction.DESC, "id"));
    }
}
