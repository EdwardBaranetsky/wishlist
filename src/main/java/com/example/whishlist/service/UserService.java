package com.example.whishlist.service;

import com.example.whishlist.entity.User;
import com.example.whishlist.repository.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;

@Service
public class UserService implements UserDetailsService {

    @Autowired
    private UserRepository repository;
    @Autowired
    private BCryptPasswordEncoder encoder;

    public long createUser(User user) {
        if (repository.existsUserByUsername(user.getUsername())) {
            throw new IllegalArgumentException(
                    "Cannot create user, the user with specified username already exists: " + user.getUsername());
        }

        user = repository.save(User.builder()
                .username(user.getUsername())
                .password(encoder.encode(user.getPassword()))
                .build());

        return user.getId();
    }

    @Override
    public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
        User user = repository.getUserByUsername(username);
        if (user == null) {
            throw new UsernameNotFoundException("User was not found");
        }

        return user;
    }

    public void removeUser(User user) {
        repository.delete(user);
    }
}
