package com.example.whishlist.service;

import com.example.whishlist.entity.Product;
import com.example.whishlist.entity.User;
import com.example.whishlist.entity.Whishlist;
import com.example.whishlist.repository.ProductRepository;
import com.example.whishlist.repository.UserRepository;
import com.example.whishlist.repository.WhishlistRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.stream.Collectors;

@Service
public class WhishlistService {

    @Autowired
    private WhishlistRepository repository;
    @Autowired
    private UserRepository userRepository;
    @Autowired
    private ProductRepository productRepository;

    public Whishlist createWhishlist(Whishlist whishlist) {
        if (!userRepository.existsUserById(whishlist.getUser().getId())) {
            throw new IllegalArgumentException("User was not found by provided user id for whishlist creation");
        }

        List<Long> productListIds = whishlist.getProductList().stream()
                .map(Product::getId).collect(Collectors.toList());

        if (productRepository.selectProductsById(productListIds).size() != productListIds.size()) {
            throw new IllegalArgumentException("All or not all of provided products exists");
        }


        return repository.save(whishlist);
    }

    public Whishlist addProductToWhishlist(Whishlist whishlist, Product product) {
        whishlist.addProductId(product);
        return repository.save(whishlist);
    }

    public Whishlist removeProductFromWhishlist(Whishlist whishlist, Product product) {
        whishlist.removeProductId(product);
        return repository.save(whishlist);
    }

    public void removeWhishlist(long whishlistId) {
        repository.deleteById(whishlistId);
    }

    public Whishlist getWhishlistById(long id) {
        return repository.getWhishlistById(id);
    }

    public List<Whishlist> selectUserWhishlist(User user, int page, int pageSize) {
        return repository.findAllByUser(user, PageRequest.of(page, pageSize, Sort.Direction.DESC, "id"));
    }
}
