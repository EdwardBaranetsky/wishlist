package com.example.whishlist.util;

/**
 * todo: review enum vs interface constants for application mode.
 */
public interface ApplicationMode {
    String TEST = "test";
    String PRODUCTION = "production";
}
