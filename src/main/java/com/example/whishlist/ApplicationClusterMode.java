package com.example.whishlist;

import com.example.whishlist.util.ApplicationMode;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

/**
 * Bean which handles spring profiles active property.
 * Provides current application mode.
 */
@Component
public class ApplicationClusterMode {

    @Value("${spring.profiles.active: " + ApplicationMode.PRODUCTION + "}")
    private String mode;

    public boolean isTestModeEnabled() {
        return mode.equals(ApplicationMode.TEST);
    }
}
