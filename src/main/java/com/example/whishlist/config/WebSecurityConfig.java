package com.example.whishlist.config;

import com.example.whishlist.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;

/**
 * Main spring security configuration, setups password encoder and routes protection.
 */
@Configuration
@EnableWebSecurity
public class WebSecurityConfig extends WebSecurityConfigurerAdapter {

    @Autowired
    private UserService userService;

    @Bean
    public BCryptPasswordEncoder bCryptPasswordEncoder() {
        return new BCryptPasswordEncoder();
    }

    @Override
    protected void configure(HttpSecurity httpSecurity) throws Exception {
        httpSecurity
                // probably should be improved for real project
                .httpBasic()
                .and()
                .csrf()
                .disable()
                .authorizeRequests()
                //access only for not registered users
                .antMatchers("/register", "/user/remove").not().fullyAuthenticated()
                .antMatchers("/").hasRole("USER")
                //access for all users
                .antMatchers("/", "/resources/**").permitAll()
                // all other pages require authentication
                .anyRequest().authenticated()
                .and()
                // setup for login
                .formLogin()
                .loginPage("/login")
                // will redirect to main page after successful login.
                .defaultSuccessUrl("/")
                .permitAll()
                .and()
                .logout()
                .permitAll()
                .logoutSuccessUrl("/");
    }

    @Autowired
    protected void configureGlobal(AuthenticationManagerBuilder auth) throws Exception {
        auth.userDetailsService(userService).passwordEncoder(bCryptPasswordEncoder());
    }
}
