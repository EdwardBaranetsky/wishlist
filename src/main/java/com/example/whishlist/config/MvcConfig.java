package com.example.whishlist.config;

import org.springframework.context.annotation.Configuration;
import org.springframework.web.servlet.config.annotation.ViewControllerRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;

/**
 * Assigns pages to routes, is needed for default spring security login route resolution
 */
@Configuration
public class MvcConfig implements WebMvcConfigurer {

    @Override
    public void addViewControllers(ViewControllerRegistry registry) {
        // registers login view for /login route, this route has default handler from spring security.
        registry.addViewController("/login").setViewName("login");
    }
}
