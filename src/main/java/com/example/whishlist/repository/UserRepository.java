package com.example.whishlist.repository;

import com.example.whishlist.entity.User;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface UserRepository extends CrudRepository<User, Long> {

    User getUserByUsername(String login);

    boolean existsUserById(long id);

    boolean existsUserByUsername(String username);
}
