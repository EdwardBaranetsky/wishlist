package com.example.whishlist.repository;

import com.example.whishlist.entity.User;
import com.example.whishlist.entity.Whishlist;
import org.springframework.data.domain.Pageable;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface WhishlistRepository extends PagingAndSortingRepository<Whishlist, Long> {

    Whishlist getWhishlistById(long id);

    List<Whishlist> findAllByUser(User user, Pageable pageable);
}
