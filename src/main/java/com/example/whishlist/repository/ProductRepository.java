package com.example.whishlist.repository;

import com.example.whishlist.entity.Product;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface ProductRepository extends PagingAndSortingRepository<Product, Long> {

    @Query(value = "select * from products where id in(:productsIds)", nativeQuery = true)
    List<Product> selectProductsById(@Param("productsIds") List<Long> productIdsList);

    Product getProductById(long id);

    boolean existsProductByName(String name);

    List<Product> findAllBy(Pageable pageable);
}
