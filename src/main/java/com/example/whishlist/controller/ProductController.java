package com.example.whishlist.controller;

import com.example.whishlist.entity.Product;
import com.example.whishlist.service.ProductService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
public class ProductController {

    @Autowired
    private ProductService service;

    @Value("${MAX_PRODUCT_PAGE_SIZE}")
    private int maxProductPageSize;

    @PostMapping("/product/create")
    public ResponseEntity createProduct(@RequestBody Product product) {

        if (product == null) {
            return ResponseEntity.badRequest().body("No valuable data was received for product creation");
        }
        if (product.getName().isEmpty()) {
            return ResponseEntity.badRequest().body("Product name cannot be empty");
        }

        // potentially could go more validations here ...........
        try {
            return ResponseEntity.ok().body(service.createProduct(product));
        } catch (IllegalArgumentException e) {
            return ResponseEntity.badRequest().body("Failed to create product cause: " + e.getMessage());
        }
    }

    @GetMapping("/product/list")
    public ResponseEntity showProductList(@RequestParam("page") int page, @RequestParam("pageSize") int pageSize) {
        if (pageSize > maxProductPageSize) {
            pageSize = maxProductPageSize;
        }
        // potentially could go more validations here ...........

        return ResponseEntity.ok().body(service.selectProductList(page, pageSize));
    }
}
