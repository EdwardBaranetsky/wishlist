package com.example.whishlist.controller;

import com.example.whishlist.ApplicationClusterMode;
import com.example.whishlist.entity.Product;
import com.example.whishlist.entity.User;
import com.example.whishlist.entity.Whishlist;
import com.example.whishlist.service.ProductService;
import com.example.whishlist.service.WhishlistService;
import com.example.whishlist.util.ApplicationMode;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.web.bind.annotation.*;

import java.security.Principal;

@RestController
@RequestMapping("/whishlist")
public class WhishlistController {

    @Autowired
    private WhishlistService service;
    @Autowired
    private ProductService productService;
    @Autowired
    private ApplicationClusterMode clusterMode;

    @Value("${MAX_PRODUCTS_SIZE_AT_WHISLIST_CREATION}")
    private int maxProductsSizeAtWhislistCreation;
    @Value("${MAX_WHISHLIST_PAGE_SIZE}")
    private int maxWhishlistPageSize;

    @PostMapping("/create")
    public ResponseEntity createWhishlist(@RequestBody Whishlist whishlist) {
        if (whishlist == null) {
            return ResponseEntity.badRequest().body("No valuable data was received for whishlist creation");
        }
        if(whishlist.getTitle().isEmpty()) {
            return ResponseEntity.badRequest().body("Title of a whishlist could not be empty");
        }
        if (whishlist.getProductList().size() > maxProductsSizeAtWhislistCreation) {
            return ResponseEntity.badRequest().body(
                    String.format("Whishlist cannot contain more than %d products at creation time",
                            maxProductsSizeAtWhislistCreation));
        }

        // potentially could go more validations here ...........

        try {
            return ResponseEntity.ok().body(service.createWhishlist(whishlist));
        } catch (IllegalArgumentException e) {
            return ResponseEntity.badRequest().body("Failed to create whishlist cause: " + e.getMessage());
        }
    }

    @PostMapping("/addProduct")
    public ResponseEntity addProductToWhishlist(
            @RequestParam("whishlistId") long whishlistId, @RequestParam("productId") long productId) {

        return makeActionOnProductsList(whishlistId, productId, true);
    }

    @PostMapping("/removeProduct")
    public ResponseEntity removeProductFromWhishlist(
            @RequestParam("whishlistId") long whishlistId, @RequestParam("productId") long productId) {

        return makeActionOnProductsList(whishlistId, productId, false);
    }

    private ResponseEntity makeActionOnProductsList(long whishlistId, long productId, boolean addProduct) {
        Product product = productService.getProductById(productId);
        if (product == null) {
            return ResponseEntity.badRequest().body("Product with provided product id does not exist");
        }

        Whishlist whishlist = service.getWhishlistById(whishlistId);
        if (whishlist == null) {
            return ResponseEntity.badRequest().body("Whishlist with provided whishlist id does not exist");
        }

        if (addProduct) {
            return ResponseEntity.ok().body(service.addProductToWhishlist(whishlist, product));
        }

        return ResponseEntity.ok().body(service.removeProductFromWhishlist(whishlist, product));
    }

    @GetMapping("/")
    public ResponseEntity selectWhishlistsForUser(@RequestParam("page") int page, @RequestParam("pageSize") int pageSize) {

        if (pageSize > maxWhishlistPageSize) {
            pageSize = maxWhishlistPageSize;
        }

        User user = (User)SecurityContextHolder.getContext().getAuthentication().getPrincipal();

        return ResponseEntity.ok().body(service.selectUserWhishlist(user, page, pageSize));
    }

    @PostMapping("/remove")
    public ResponseEntity removeWhishlist(@RequestParam("whishlistId") long id) {
        // if speaking about production here should appear ability for admin to force the rule and delete user,
        // could be resolved via roles and spring securities
        if (!clusterMode.isTestModeEnabled()) {
            return ResponseEntity.badRequest().body("Method is not available");
        }

        service.removeWhishlist(id);

        return ResponseEntity.ok().build();
    }
}
