package com.example.whishlist.controller;

import com.example.whishlist.ApplicationClusterMode;
import com.example.whishlist.entity.User;
import com.example.whishlist.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class UserController {

    @Autowired
    private UserService service;
    @Autowired
    private ApplicationClusterMode clusterMode;

    @PostMapping("/register")
    public ResponseEntity register(@RequestBody User user) {

        if (user == null) {
            return ResponseEntity.badRequest().body("No valuable data was received for registration");
        }
        if (user.getPassword().isEmpty()){
            return ResponseEntity.badRequest().body("Password could not be empty");
        }
        if (!user.getConfirmPassword().equals(user.getPassword())) {
            return ResponseEntity.badRequest().body("Password and confirm password does not match");
        }

        // potentially could go more validations here ...........

        try {
            return ResponseEntity.ok().body(service.createUser(user));
        } catch (IllegalArgumentException e) {
            return ResponseEntity.badRequest().body("Failed to create user cause: " + e.getMessage());
        }
    }

    @PostMapping("/user/remove")
    public ResponseEntity removeUser(@RequestBody User user) {
        // if speaking about production here should appear ability for admin to force the rule and delete user,
        // could be resolved via roles and spring securities
        if (!clusterMode.isTestModeEnabled()) {
            return ResponseEntity.badRequest().body("Method is not available");
        }

        service.removeUser(user);

        return ResponseEntity.ok().build();
    }
}
