package com.example.whishlist.controller;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class HomeController {

    @Value("${HOME_PAGE_GREETING}")
    private String homePageGreeting;

    @GetMapping("/")
    public String home() {
        return homePageGreeting;
    }
}
