package com.example.whishlist.entity;

import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;

import javax.persistence.*;
import java.util.Collection;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;

@Entity
@AllArgsConstructor
@NoArgsConstructor
@Builder
@Data
@Table(name = "users")
public class User implements UserDetails {

    // should be improved for real project via creating roles table
    // and of course if project requires more roles than one.
    @JsonIgnore
    private static List<? extends GrantedAuthority> authorities =
            Stream.of((GrantedAuthority)() -> "USER").collect(Collectors.toList());

    @Id
    @GeneratedValue
    private Long id;

    @Column(unique = true)
    private String username;

    private String password;

    @Transient
    private String confirmPassword;

    @JsonIgnore
    @Override
    public Collection<? extends GrantedAuthority> getAuthorities() {
        return authorities;
    }

    @Override
    public boolean isAccountNonExpired() {
        // could be improved for real project
        return true;
    }

    @Override
    public boolean isAccountNonLocked() {
        // could be improved for real project
        return true;
    }

    @Override
    public boolean isCredentialsNonExpired() {
        // could be improved for real project
        return true;
    }

    @Override
    public boolean isEnabled() {
        // could be improved for real project
        return true;
    }
}
