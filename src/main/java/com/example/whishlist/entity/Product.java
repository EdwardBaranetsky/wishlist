package com.example.whishlist.entity;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;

@Entity
@AllArgsConstructor
@NoArgsConstructor
@Builder
@Data
@Table(name = "products")
public class Product {

    @Id
    @GeneratedValue
    private Long id;

    @Column(unique = true)
    private String name;

    // this is probably not required for whishlist
    // but is more realistic.
    private long price;
}
