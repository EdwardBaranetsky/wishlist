# Getting Started

* From the start of project it was made a typo in word wish,
  that's why we have whishlist instead of wishlist over the project.
  For example/test project looks like not meaningful but for real project should be fixed.

### project description 
It is an example of spring boot project under docker containers.
The solution contains only rest api and auth page which provides spring security.
All routes are authorized by http basic authentication - which could be changed for real project.
Application does use postgres as database which could be easily changed
in application.properties - spring boot property file. 


### build/run/test
In order to start containers the next command should be run:
* docker-compose up

If it is needed to rebuild images/containers, please run:
* docker-compose up --build

Test in this example should started locally(a point for improvement for real project).
So to run test a db container should be run via next command: 
* docker-compose up postgres

After that please run test via gradle or intellij idea.

### CLI - wish
Project has a cli tool written on php - whish


